using System.Collections;
using System.Collections.Generic;
using ParkingAssistant.Models;

namespace ParkingAssistant.UnitTests.ClassData
{
    public class JumboClassData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { new Jumbo { Model = "A380" } };
            yield return new object[] { new Jumbo { Model = "B747" } };
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}