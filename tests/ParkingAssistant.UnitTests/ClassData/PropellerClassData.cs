using System.Collections;
using System.Collections.Generic;
using ParkingAssistant.Models;

namespace ParkingAssistant.UnitTests.ClassData
{
    public class PropellerClassData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { new Propeller { Model = "E195" } };
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}