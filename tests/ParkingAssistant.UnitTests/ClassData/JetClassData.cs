using System.Collections;
using System.Collections.Generic;
using ParkingAssistant.Models;

namespace ParkingAssistant.UnitTests.ClassData
{
    public class JetClassData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { new Jet { Model = "A330" } };
            yield return new object[] { new Jet { Model = "B777" } };
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}