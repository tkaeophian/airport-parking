using Microsoft.Extensions.Logging;
using Moq;
using ParkingAssistant.Enums;
using ParkingAssistant.Interfaces;
using ParkingAssistant.Services;

namespace ParkingAssistant.UnitTests.Fixtures
{
    public class JumboFixture {
        public IParkingService ParkingService { get; private set; }
        public JumboFixture()
        {
            var mockLogger = new Mock<ILogger<JumboParkingSpace>>();
            ParkingService = new JumboParkingSpace(mockLogger.Object);
            ParkingService.Initialize(25, AircraftTypes.Jumbo);
        }
    }
}