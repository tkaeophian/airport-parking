using Microsoft.Extensions.Logging;
using Moq;
using ParkingAssistant.Enums;
using ParkingAssistant.Interfaces;
using ParkingAssistant.Services;

namespace ParkingAssistant.UnitTests.Fixtures
{
    public class JetFixture {
        public IParkingService ParkingService { get; private set; }
        public JetFixture()
        {
            var mockLogger = new Mock<ILogger<JetParkingSpace>>();
            ParkingService = new JetParkingSpace(mockLogger.Object);
            ParkingService.Initialize(50, AircraftTypes.Jet);
        }
    }
}