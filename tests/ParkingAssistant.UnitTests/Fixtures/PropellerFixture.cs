using Microsoft.Extensions.Logging;
using Moq;
using ParkingAssistant.Enums;
using ParkingAssistant.Interfaces;
using ParkingAssistant.Services;

namespace ParkingAssistant.UnitTests.Fixtures
{
    public class PropellerFixture {
        public IParkingService ParkingService { get; private set; }
        public PropellerFixture()
        {
            var mockLogger = new Mock<ILogger<PropellerParkingSpace>>();
            ParkingService = new PropellerParkingSpace(mockLogger.Object);
            ParkingService.Initialize(25, AircraftTypes.Propeller);
        }
    }
}