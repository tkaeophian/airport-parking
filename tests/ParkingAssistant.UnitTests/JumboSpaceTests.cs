using System.Linq;
using ParkingAssistant.Enums;
using ParkingAssistant.Models;
using ParkingAssistant.UnitTests.ClassData;
using ParkingAssistant.UnitTests.Fixtures;
using Xunit;

namespace ParkingAssistant.UnitTests
{
    public class JumboSpaceTests : IClassFixture<JumboFixture>
    {
        private readonly JumboFixture _fixture;

        public JumboSpaceTests(JumboFixture fixture)
        {
            _fixture = fixture;
        }

        [Theory]
        [ClassData(typeof(JumboClassData))]
        public void JumbosAircraft_CanUse_JumboSpace(Jumbo aircraft)
        {
            // arrange
            
            // assert
            var getSpace = _fixture.ParkingService.GetParkingSpace(aircraft);
            var result = _fixture.ParkingService.OnArrival(aircraft, getSpace);
            
            // act
            Assert.True(result);
        }
        
        [Theory]
        [ClassData(typeof(JetClassData))]
        public void JetAircraft_CanUse_JumboSpace(Jet aircraft)
        {
            // arrange
            
            // assert
            var getSpace = _fixture.ParkingService.GetParkingSpace(aircraft);
            var result = _fixture.ParkingService.OnArrival(aircraft, getSpace);
            
            // act
            Assert.True(result);
        }
        
        [Theory]
        [ClassData(typeof(PropellerClassData))]
        public void PropellerAircraft_CanUse_JumboSpace(Propeller aircraft)
        {
            // arrange
            
            // assert
            var getSpace = _fixture.ParkingService.GetParkingSpace(aircraft);
            var result = _fixture.ParkingService.OnArrival(aircraft, getSpace);
            
            // act
            Assert.True(result);
        }

        [Fact]
        public void OnJumboAircraft_Departure_Should_Increment_Space()
        {
            // arrange
            var occupiedSpaces = _fixture.ParkingService.OccupiedSpaces().ToList();
            
            // assert
            foreach (var space in occupiedSpaces)
            {
                _fixture.ParkingService.OnDeparture(space); 
            }

            // act
            Assert.Equal(25,_fixture.ParkingService.AvailableSpaces().Count);
        }
        
        [Fact]
        public void Unavailable_Slot_Should_Display_Error()
        {
            // arrange
            var result = new ParkingSpace();
            var aircraft = new Jumbo
            {
                Type = AircraftTypes.Jumbo,
                Model = "A380"
            };

            // assert
            for (var i = 0; i < 26; i++)
            {
                result = _fixture.ParkingService.GetParkingSpace(aircraft);
                if (!result.IsAvailable)
                {
                    break;
                }
                
                _fixture.ParkingService.OnArrival(aircraft, result);
            }
            
            // act
            Assert.Equal("No Parking Slot Available", result.Message);
        }
    }
}