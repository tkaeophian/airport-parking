using System.Linq;
using ParkingAssistant.Models;
using ParkingAssistant.UnitTests.ClassData;
using ParkingAssistant.UnitTests.Fixtures;
using Xunit;

namespace ParkingAssistant.UnitTests
{
    public class JetSpaceTests : IClassFixture<JetFixture>
    {
        private readonly JetFixture _fixture;

        public JetSpaceTests(JetFixture fixture)
        {
            _fixture = fixture;
        }

        [Theory]
        [ClassData(typeof(JumboClassData))]
        public void JumbosAircraft_ShouldNotUse_JetSpace(Jumbo aircraft)
        {
            // arrange
            
            // assert
            var result = _fixture.ParkingService.GetParkingSpace(aircraft);

            // act
            Assert.False(result.IsAvailable);
        }
        
        [Theory]
        [ClassData(typeof(JetClassData))]
        public void JetAircraft_CanUse_JetSpace(Jet aircraft)
        {
            // arrange
            
            // assert
            var getSpace = _fixture.ParkingService.GetParkingSpace(aircraft);
            var result = _fixture.ParkingService.OnArrival(aircraft, getSpace);
            
            // act
            Assert.True(result);
        }
        
        [Theory]
        [ClassData(typeof(PropellerClassData))]
        public void PropellerAircraft_CanUse_JetSpace(Propeller aircraft)
        {
            // arrange
            
            // assert
            var getSpace = _fixture.ParkingService.GetParkingSpace(aircraft);
            var result = _fixture.ParkingService.OnArrival(aircraft, getSpace);
            
            // act
            Assert.True(result);
        }
        
        [Fact]
        public void OnJetAircraft_Departure_Should_Increment_Space()
        {
            // arrange
            var occupiedSpaces = _fixture.ParkingService.OccupiedSpaces().ToList();
            
            // assert
            foreach (var space in occupiedSpaces)
            {
                _fixture.ParkingService.OnDeparture(space); 
            }

            // act
            Assert.Equal(50,_fixture.ParkingService.AvailableSpaces().Count);
        }
    }
}