using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using ParkingAssistant.Models;

namespace ParkingAssistant.Services
{
    public class JetParkingSpace : ParkingService
    {
        protected override List<string> AircraftModels { get; set; } = new List<string>{ "A330", "B777", "E195" };
        
        public JetParkingSpace(ILogger<JetParkingSpace> logger) : base(logger)
        {
            
        }
    }
}