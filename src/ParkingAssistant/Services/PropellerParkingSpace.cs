using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using ParkingAssistant.Models;

namespace ParkingAssistant.Services
{
    public class PropellerParkingSpace : ParkingService
    {
        protected override List<string> AircraftModels { get; set; } = new List<string>{ "E195" };
        
        public PropellerParkingSpace(ILogger<PropellerParkingSpace> logger) : base(logger)
        {
            
        }
    }
}