using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using ParkingAssistant.Models;

namespace ParkingAssistant.Services
{
    public class JumboParkingSpace : ParkingService
    {
        protected override List<string> AircraftModels { get; set; } = new List<string>{ "A380", "B747", "A330", "B777", "E195" };
        
        public JumboParkingSpace(ILogger<JumboParkingSpace> logger) : base(logger)
        {

        }
    }
}