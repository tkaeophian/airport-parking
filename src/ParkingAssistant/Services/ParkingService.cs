using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using ParkingAssistant.Enums;
using ParkingAssistant.Interfaces;
using ParkingAssistant.Models;

namespace ParkingAssistant.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ILogger _logger;

        public ParkingService(
            ILogger<ParkingService> logger)
        {
            _logger = logger;
            ParkingSpaces = new List<ParkingSpace>();
            OccupiedSpaces = new List<ParkingSpace>();
        }

        protected virtual List<string> AircraftModels { get; set; }
        private List<ParkingSpace> ParkingSpaces { get; set; }
        private List<ParkingSpace> OccupiedSpaces { get; set; }

        public virtual void Initialize(int maxSpaces, AircraftTypes aircraftType)
        {
            for (var i = 0; i < maxSpaces; i++)
            {
                ParkingSpaces.Add(new ParkingSpace
                {
                    IsAvailable = true,
                    Position = i,
                    SpaceType = aircraftType
                });
            }
        }

        public ParkingSpace GetParkingSpace(IAircraft aircraft)
        {
            var parkingSpace = new ParkingSpace();

            var checkParkingSpace = GetSmallestParkingSpaceRequired(aircraft);

            if (checkParkingSpace == null)
            {
                parkingSpace.Message = "Invalid Aircraft Type";
                return parkingSpace;
            }

            var getParkingSpace =
                ParkingSpaces.FirstOrDefault(x => x.SpaceType >= checkParkingSpace.SpaceType && x.IsAvailable);
            if (getParkingSpace != null && AircraftModels.Contains(aircraft.Model))
            {
                return getParkingSpace;
            }

            parkingSpace.Message = "No Parking Slot Available";
            return parkingSpace;
        }

        public bool OnArrival(IAircraft aircraft, ParkingSpace parkingSpace)
        {
            if (OccupiedSpaces.Contains(parkingSpace))
            {
                throw new InvalidOperationException($"Aircraft with number {aircraft.Model} is already parked at space {parkingSpace.Position}");
            }
            
            ParkingSpaces.Remove(parkingSpace);

            parkingSpace.Aircraft = aircraft;
            OccupiedSpaces.Add(parkingSpace);

            return true;
        }

        public void OnDeparture(ParkingSpace parkingSpace)
        {
            ParkingSpaces.Add(parkingSpace);
            OccupiedSpaces.Remove(parkingSpace);
        }

        public IList<ParkingSpace> AvailableSpaces()
        {
            return ParkingSpaces;
        }

        IList<ParkingSpace> IParkingService.OccupiedSpaces()
        {
            return OccupiedSpaces;
        }

        private ParkingSpace GetSmallestParkingSpaceRequired(IAircraft aircraft)
        {
            switch (aircraft.Type)
            {
                case AircraftTypes.Propeller:
                    return new ParkingSpace() { SpaceType = AircraftTypes.Propeller };
                case AircraftTypes.Jet:
                    return new ParkingSpace() { SpaceType = AircraftTypes.Jet };
                case AircraftTypes.Jumbo:
                    return new ParkingSpace() { SpaceType = AircraftTypes.Jumbo };
                default:
                    throw new ArgumentException($"Aircraft type {aircraft.Type} is invalid.");
            }
        }
    }
}