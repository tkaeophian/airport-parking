using ParkingAssistant.Enums;
using ParkingAssistant.Interfaces;

namespace ParkingAssistant.Models
{
    public class Jet : IAircraft
    {
        public string Model { get; set; }
        public AircraftTypes Type { get; set; } = AircraftTypes.Jet;
    }
}