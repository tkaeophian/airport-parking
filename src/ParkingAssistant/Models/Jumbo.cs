using ParkingAssistant.Enums;
using ParkingAssistant.Interfaces;

namespace ParkingAssistant.Models
{
    public class Jumbo : IAircraft
    {
        public string Model { get; set; }
        public AircraftTypes Type { get; set; } = AircraftTypes.Jumbo;
    }
}