using ParkingAssistant.Enums;
using ParkingAssistant.Interfaces;

namespace ParkingAssistant.Models
{
    public class ParkingSpace
    {
        public int Position { get; set; }
        public bool IsAvailable { get; set; }
        public string Message { get; set; }
        public AircraftTypes SpaceType { get; set; }
        public IAircraft Aircraft { get; set; }
        
    }
}