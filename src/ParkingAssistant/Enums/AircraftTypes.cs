namespace ParkingAssistant.Enums
{
    public enum AircraftTypes
    {
        Propeller = 0,
        Jet = 1,
        Jumbo = 2
    }
}