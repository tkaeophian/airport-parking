using System.Collections.Generic;
using ParkingAssistant.Enums;
using ParkingAssistant.Models;

namespace ParkingAssistant.Interfaces
{
    public interface IParkingService
    {
        void Initialize(int maxSpaces, AircraftTypes aircraftType);
        ParkingSpace GetParkingSpace(IAircraft aircraft);
        bool OnArrival(IAircraft aircraft, ParkingSpace parkingSpace);
        void OnDeparture(ParkingSpace parkingSpace);
        IList<ParkingSpace> AvailableSpaces();
        IList<ParkingSpace> OccupiedSpaces();
    }
}