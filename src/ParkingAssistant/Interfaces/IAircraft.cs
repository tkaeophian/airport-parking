using ParkingAssistant.Enums;

namespace ParkingAssistant.Interfaces
{
    public interface IAircraft
    {
        string Model { get; set; }
        AircraftTypes Type { get; set; }
    }
}